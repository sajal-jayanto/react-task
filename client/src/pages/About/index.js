import React from 'react';
import { Card } from 'antd';
import MyLayout from '../../layout/index';

const AboutPage = () => {

    return (
        <MyLayout>
            <div style={{minHeight : '75vh'}} className="container">
                <div className="site-card-border-less-wrapper">
                    <Card style={{ textAlign : 'center'}} className="mt-3" bordered={false} >
                        <h3> About Us </h3>    
                        <p>Enterprise Rent-A-Car is an ongoing American success story. Our guiding principles, and humble beginning, revolve around personal honesty and integrity. We believe in strengthening our communities one neighborhood at a time, serving our customers as if they were our family, and rewarding hard work. These things are as true today as they were when we were founded in 1957.</p>
                    </Card>
                </div>
            </div>
           
        </MyLayout>
    );

};

export default AboutPage;
