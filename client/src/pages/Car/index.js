import React , { useState , useEffect } from 'react';
import { useDispatch , connect } from 'react-redux';
import { Menu, Dropdown, Button, Row , Col , Card , Divider , Input } from 'antd';
import { DownOutlined, MoneyCollectTwoTone } from '@ant-design/icons';
import MyLayout from '../../layout/index';
import Listcar from '../../components/car/Listcar';
import { listCarAction } from '../../redux/car/list/listCarAction';
import AddCar from '../../components/car/Addcar';
import axios from 'axios';

const HomePage = ({ listCarData }) => {

    const dispatch = useDispatch();

    const [ searchWord , setSearchWord ] = useState("");
    const [ selectDropdown , setSelectDropdown ] = useState("USD");
    const [ addCarVisible , setaddCarVisible ] = useState(false);
    const [ currency , setCurrency ] = useState({});


    useEffect(() => {

        const fatchData = async () => {

            setSelectDropdown("USD");
            await listCarAction(dispatch , { word : searchWord });

            /// Don't have time so can't use redux 
            const res = await axios.get(`https://api.exchangeratesapi.io/latest?base=USD&symbols=USD,EUR,GBP,JPY`);
            const data = res.data.rates;
            setCurrency(data)
        }
        
        fatchData();

    }, [searchWord , dispatch]);

    const handleMenuClick = (e) => {
        const selectItem = e.key;
        if(selectDropdown !== selectItem){
            setSelectDropdown(selectItem);
            listCarData.carList.map(car => {
                return car.newprice = Number(car.price) * Number(currency[selectItem.toString()]);
            });
        }
    }

    const menu = (
        <Menu onClick={handleMenuClick}>
            <Menu.Item key="USD" icon={<MoneyCollectTwoTone />}>USD</Menu.Item>
            <Menu.Item key="EUR" icon={<MoneyCollectTwoTone />}>EUR</Menu.Item>
            <Menu.Item key="GBP" icon={<MoneyCollectTwoTone />}>GBP</Menu.Item>
            <Menu.Item key="JPY" icon={<MoneyCollectTwoTone />}>JPY</Menu.Item>
        </Menu>
    );

    return (
        <MyLayout>
            <div className="container">
                <div className="site-card-border-less-wrapper">
                    <Card className="mt-3" bordered={false} >
                        <h4> Search car </h4>
                        <Row gutter={[8, 8]}>
                            <Col span={12} > 
                                <Input placeholder="Car Brand Name" allowClear onChange={(e) => setSearchWord(e.target.value)} /> 
                            </Col>
                            <Col span={12} > 
                                <Dropdown overlay={menu}>
                                    <Button> {selectDropdown} <DownOutlined /> </Button>
                                </Dropdown>
                            </Col>
                        </Row>
                        <Divider />
                        <Row gutter={[8, 8]}>
                            <Col span={24}>
                                <div className="new-car">
                                    <Button type="primary" onClick={ () => setaddCarVisible(true) } > 
                                        Add New Car 
                                    </Button>
                                </div>
                            </Col>
                            <Listcar listCarData={listCarData}/>   
                        </Row>
                        
                        
                        
                    </Card>
                </div>
            </div>

            <AddCar addCarVisible={addCarVisible} setaddCarVisible={setaddCarVisible}/>

        </MyLayout>
    );

};

const mapStateTopProps = state => { 
    return {
        listCarData : state.listCarData
    };
}

export default connect(mapStateTopProps , null)(HomePage);

