import { notification} from 'antd';


export const notificationWithIcon = (type , tital , description)=> {
    
    notification[type]({
        message: tital,
        description: description,
    });

};
