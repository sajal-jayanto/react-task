import React from 'react';
import { BrowserRouter , Switch  } from 'react-router-dom';

// Routes
// import PrivateRoute from '../routes/PrivateRoutes';
import PublicRoute from '../routes/PublicRoutes';

// pages
import HomePage from '../pages/Car/index';
import AboutPage from '../pages/About/index';


const Routers = () => {

    return(
        <BrowserRouter>
            <Switch>

                <PublicRoute exact restricted={true}  path="/" component={HomePage} />
                <PublicRoute exact restricted={true}  path="/about" component={AboutPage} />

                
                {/* <PrivateRoute path="/compare" component={ComparePage} />    */}

            </Switch>
        </BrowserRouter>
    );
};

export default Routers;
