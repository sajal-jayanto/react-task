import { createStore , applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducers  from './rootReducer';
import logger from 'redux-logger';

const store = createStore(rootReducers , applyMiddleware(thunk , logger));

export default store;
