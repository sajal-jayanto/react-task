import { 
    
    DELETE_CAR_REQUEST_SUBMIT,
    DELETE_CAR_REQUEST_SUCCESS,
    DELETE_CAR_REQUEST_ERROR

} from './DeleteCarType';


const initialState = {

    canDelete : false,
    loding : false,
    error : false

};

const deleteCarReducer = (state = initialState , action) => {

    switch(action.type){
        case DELETE_CAR_REQUEST_SUBMIT  : {
            
            const newState = { ...state };
            
            newState.loding = true;
            newState.error = false;

            return newState;

        }
        case DELETE_CAR_REQUEST_SUCCESS : {

            const newState = { ...state };

            newState.loding = false;
            newState.canDelete = action.payload;
            newState.error = false;

            return newState;

        }
        case DELETE_CAR_REQUEST_ERROR : {
            
            const newState = { ...state };

            newState.loding = false;
            newState.error = true;
            
            return newState;

        }
        default : {
            return state;
        }
    }

}

export default deleteCarReducer;
