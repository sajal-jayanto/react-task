import axios from 'axios';
import { 
    
    DELETE_CAR_REQUEST_SUBMIT,
    DELETE_CAR_REQUEST_SUCCESS,
    DELETE_CAR_REQUEST_ERROR

} from './DeleteCarType';

export const deleteCarAction = async (dispatch , carId) => {

    try {
        dispatch({ type : DELETE_CAR_REQUEST_SUBMIT });
        
        const res = await axios.delete(`http://localhost:5000/api/v1/car/delete/${carId}`);
        const canDelete = res.data.data; 
        
        if(canDelete){
            dispatch({ type : DELETE_CAR_REQUEST_SUCCESS , payload : canDelete });
            return true;
        }
        return false;
    }
    catch(error){
        dispatch({ type : DELETE_CAR_REQUEST_ERROR });
        return false;
    }
}
