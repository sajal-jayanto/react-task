import { 
    
    ADD_CAR_REQUEST_SUBMIT,
    ADD_CAR_REQUEST_SUCCESS,
    ADD_CAR_REQUEST_ERROR

} from './addCarType';


const initialState = {

    carAdd : null,
    loding : false,
    error : false

};

const carReducer = (state = initialState , action) => {

    switch(action.type){
        case ADD_CAR_REQUEST_SUBMIT  : {
            
            const newState = { ...state };
            
            newState.loding = true;
            newState.error = false;

            return newState;

        }
        case ADD_CAR_REQUEST_SUCCESS : {

            const newState = { ...state };

            newState.loding = false;
            newState.carAdd = action.payload;
            newState.error = false;

            return newState;

        }
        case ADD_CAR_REQUEST_ERROR : {
            
            const newState = { ...state };

            newState.loding = false;
            newState.error = true;
            
            return newState;

        }
        default : {
            return state;
        }
    }

}

export default carReducer;
