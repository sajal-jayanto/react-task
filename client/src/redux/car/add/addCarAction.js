import axios from 'axios';
import { 
    
    ADD_CAR_REQUEST_SUBMIT,
    ADD_CAR_REQUEST_SUCCESS,
    ADD_CAR_REQUEST_ERROR

} from './addCarType';

export const carAddAction = async (dispatch , formData) => {

    try {
        dispatch({ type : ADD_CAR_REQUEST_SUBMIT });

        const res = await axios.post(`http://localhost:5000/api/v1/car/create` , formData , { headers: { "Content-Type": "multipart/form-data" } } );
        const carInfo = res.data; 
        
        dispatch({ type : ADD_CAR_REQUEST_SUCCESS , payload : carInfo });
        
        return true;
    }
    catch(error){
        
        dispatch({ type : ADD_CAR_REQUEST_ERROR });
        return false;
    }
}
