import axios from 'axios';
import { 
    
    SHOW_CAR_REQUEST_SUBMIT,
    SHOW_CAR_REQUEST_SUCCESS,
    SHOW_CAR_REQUEST_ERROR

} from './showCarType';

export const ShowCarAction = async (dispatch , carId ) => {

    try {
        dispatch({ type : SHOW_CAR_REQUEST_SUBMIT });

        const res = await axios.get(`http://localhost:5000/api/v1/car/show/${carId}`);
        const carInfo = res.data; 
        
        dispatch({ type : SHOW_CAR_REQUEST_SUCCESS , payload : carInfo });
        
        return true;
    }
    catch(error){
        
        dispatch({ type : SHOW_CAR_REQUEST_ERROR });
        return false;
    }
}
