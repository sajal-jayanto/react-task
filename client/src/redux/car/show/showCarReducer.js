import { 
    
    SHOW_CAR_REQUEST_SUBMIT,
    SHOW_CAR_REQUEST_SUCCESS,
    SHOW_CAR_REQUEST_ERROR

} from './showCarType';


const initialState = {

    car : null,
    loding : false,
    error : false

};

const showCarReducer = (state = initialState , action) => {

    switch(action.type){
        case SHOW_CAR_REQUEST_SUBMIT  : {
            
            const newState = { ...state };
            
            newState.loding = true;
            newState.error = false;

            return newState;

        }
        case SHOW_CAR_REQUEST_SUCCESS : {

            const newState = { ...state };

            newState.loding = false;
            newState.car = action.payload;
            newState.error = false;

            return newState;

        }
        case SHOW_CAR_REQUEST_ERROR : {
            
            const newState = { ...state };

            newState.loding = false;
            newState.error = true;
            
            return newState;

        }
        default : {
            return state;
        }
    }

}

export default showCarReducer;
