import { 
    
    LIST_CAR_REQUEST_SUBMIT,
    LIST_CAR_REQUEST_SUCCESS,
    LIST_CAR_REQUEST_ERROR

} from './listCarType';


const initialState = {

    carList : null,
    loding : false,
    error : false

};

const listCarReducer = (state = initialState , action) => {

    switch(action.type){
        case LIST_CAR_REQUEST_SUBMIT  : {
            
            const newState = { ...state };
            
            newState.loding = true;
            newState.error = false;

            return newState;

        }
        case LIST_CAR_REQUEST_SUCCESS : {

            const newState = { ...state };

            newState.loding = false;
            newState.carList = action.payload;
            newState.error = false;

            return newState;

        }
        case LIST_CAR_REQUEST_ERROR : {
            
            const newState = { ...state };

            newState.loding = false;
            newState.error = true;
            
            return newState;

        }
        default : {
            return state;
        }
    }

}

export default listCarReducer;
