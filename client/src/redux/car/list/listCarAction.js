import axios from 'axios';
import { 
    
    LIST_CAR_REQUEST_SUBMIT,
    LIST_CAR_REQUEST_SUCCESS,
    LIST_CAR_REQUEST_ERROR

} from './listCarType';

export const listCarAction = async (dispatch , searchWord = "") => {

    try {

        dispatch({ type : LIST_CAR_REQUEST_SUBMIT });

        const res = await axios.post(`http://localhost:5000/api/v1/car/list` , searchWord);
        const carList = res.data; 
        
        dispatch({ type : LIST_CAR_REQUEST_SUCCESS , payload : carList });
        
        return true;
    }
    catch(error){
        dispatch({ type : LIST_CAR_REQUEST_ERROR });
        return false;
    }
}
