import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import addCarReducer from './car/add/addCarReducer';
import listCarReducer from './car/list/listCarReducer';
import showCarReducer from './car/show/showCarReducer';

const persistConfig = {
    key: 'root',
    storage,
    reduxStore:['addCarReducer' , 'listCarReducer' , 'showCarReducer']
}

const rootReducer = combineReducers({
    carAddData : addCarReducer,
    listCarData : listCarReducer,
    showcar : showCarReducer
});

export default persistReducer(persistConfig , rootReducer);
