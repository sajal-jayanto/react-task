import React from 'react';
import { Link } from 'react-router-dom';
import { Layout , Menu } from "antd";

const { Header } = Layout;

const Topbar = () => {

    return(
        <Header>
            <div className="logo" >
                <Link  to="/" > 
                    <h4 style={{color:'#fff'}}> Car Shop </h4>
                </Link>
            </div>
            <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
                
                <Menu.Item key="1" className="mr-1">
                    <span> Home </span>
                    <Link to='/' /> 
                </Menu.Item>
                <Menu.Item key="2"> 
                    <span> About </span>
                    <Link to='/about' /> 
                </Menu.Item>
                
            </Menu>
        </Header>
    );
}

export default Topbar;