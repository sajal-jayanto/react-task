import React from 'react';
import { useDispatch } from 'react-redux';
import { deleteCarAction } from '../../redux/car/Delete/DeleteCarAction';
import { Image } from 'antd';
import { notificationWithIcon } from '../../util/notification';
import { listCarAction } from '../../redux/car/list/listCarAction';

import {Button, Drawer } from 'antd';


const Showcar = ({ showDitels , setShowDitels , selectedItem }) => {

    const dispatch = useDispatch();
    
    const editItem = () => {

        //// can't do for time  
        console.log("Edit" , selectedItem._id);
        notificationWithIcon("error" , "Can't Finnestad For Time" ,  "");
    }

    const deleteItem = async () => {

        const res = await deleteCarAction(dispatch , selectedItem._id);

        if(res){
            setShowDitels(false);
            await listCarAction(dispatch);
            notificationWithIcon("success" , "Delete Item");
        }
        else{
            notificationWithIcon("error" , "Can't deleted");
        }
    }

    return (
        <React.Fragment>
            <Drawer
                title="Car Ditels"
                width = {720}
                onClose = {() => setShowDitels(false)}
                visible = {showDitels}
                bodyStyle = {{ paddingBottom: 80 }}   
                footer = {
                    <div style={{ textAlign: 'right' }} >
                        <Button onClick={editItem} style={{ marginRight: 8 }}> Edit </Button>
                        <Button type="danger" onClick={deleteItem} > Delete </Button>
                    </div>
                }
            >
                
                <Image
                    width={600}
                    height={250}
                    src="./logo512.png" 
                /> { /* {selectedItem.image} for url path from DB now use demo PIC */  }
                <div className="text-center">
                    <h4>{selectedItem?.name} </h4>
                    <p>{selectedItem?.brand} </p>
                    <p>{selectedItem?.description} </p>
                    <h5>{selectedItem?.newprice ? selectedItem?.newprice : selectedItem?.price } </h5>
                </div>
            </Drawer>
        </React.Fragment>
    );

};

export default Showcar;
