import React , { useState } from 'react';
import { useDispatch } from 'react-redux';
import { carAddAction } from '../../redux/car/add/addCarAction';
import { notificationWithIcon } from '../../util/notification';
import { Input , Button, Row , Col , Drawer , Form } from 'antd';

const AddCar = ({ addCarVisible , setaddCarVisible}) => {

    const [ name , setName ] = useState("");
    const [ brandName , setBrandName ] = useState("");
    const [ imageName , setImageName ] = useState("choose an image");
    const [ description , setDescription ] = useState(""); 
    const [ imageFile , setImageFile ] = useState(null);
    const [ price , setPrice ] = useState(""); 

    const dispatch = useDispatch();

    const fileupload = (e) => {
        setImageFile(e.target.files[0]);
        setImageName(e.target.files[0].name); 
    }

    const saveData = async () => {

        const formData = new FormData();

        formData.append('name' , name);
        formData.append('brand' , brandName);
        formData.append('description' , description);
        formData.append('price' , price);
        formData.append('file' , imageFile);

        let createCar = await carAddAction(dispatch , formData);

        if(createCar){
            
            setaddCarVisible(false);
            setName("");
            setBrandName("");
            setDescription("");
            setImageFile(null);
            setPrice("");
            
            notificationWithIcon("success" , "New Car Added" , "You create a New car");
        }
        else notificationWithIcon("error" , "Can't create car" , "Some thing is wrong");
    }

    return (
        <React.Fragment>
            <Drawer
                title="Create a new car"
                width = {720}
                onClose = {() => setaddCarVisible(false)}
                visible = {addCarVisible}
                bodyStyle = {{ paddingBottom: 80 }}
                footer = {
                    <div style={{ textAlign: 'right' }} >
                        <Button onClick={() => setaddCarVisible(false) } style={{ marginRight: 8 }}> Cancel </Button>
                        <Button type="primary" onClick={saveData}> Save </Button>
                    </div>
                }
            >
            
                <Form layout="vertical" hideRequiredMark>
                    <Row gutter={16}>
                        <Col span={24}>
                            <Form.Item name="name" label="Name">
                            <Input onChange={ (e) => setName(e.target.value) } placeholder="Please enter user name" />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={24}>
                            <Form.Item name="brandname" label="Brand name"  >
                                <Input onChange={(e) => setBrandName(e.target.value) } placeholder="Please enter user name" />
                            </Form.Item>
                        </Col>

                    </Row>

                    <Row gutter={16}>
                        <Col span={24}>
                            <Form.Item name="price" label="Price in USD"  >
                                <Input onChange={(e) => setPrice(e.target.value) } placeholder="Please enter price" />
                            </Form.Item>
                        </Col>
                    </Row>
                    
                    <Row gutter={16}>
                        <Col span={24}>
                            <label> Upload Image </label>
                            <div className="custom-file">
                                <input type="file" 
                                    className="custom-file-input" 
                                    id="validatedCustomFile" 
                                    required 
                                    onChange={fileupload} 
                                    accept="image/png, image/jpg" 
                                />
                                <label className="custom-file-label" htmlFor="validatedCustomFile">{imageName}</label>
                            </div>
                        </Col>
                    </Row>

                    <Row gutter={16}>
                        <Col span={24}>
                            <Form.Item className="mt-3" name="description" label="Description">
                            <Input.TextArea onChange={(e) => setDescription(e.target.value) } rows={5} placeholder="please enter url description" />
                            </Form.Item>
                        </Col>
                    </Row>
                    
                </Form>
            </Drawer>
        </React.Fragment>
    );

};

export default AddCar;
