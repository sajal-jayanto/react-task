import React , { useState , useEffect } from 'react';
import Showcar from './Showcar';
import {  Row , Col , Pagination , Image , Card , Spin } from 'antd';

const Listcar = ({ listCarData  }) => {

    const [ carList , setCarList ] = useState([]);
    const [ showDitels , setShowDitels ] = useState(false);
    const [ selectedItem , setSelectedItem ] = useState(null);

    const pageParItem = 5;
    
    const pageChange = (current, pageSize) => {
        let skipItem = (current - 1) * pageSize;
        let lastItem = current * pageSize;
        setCarList(listCarData.carList?.slice(skipItem , lastItem));
    }

    useEffect(() => {
        if(!listCarData.loding && listCarData.carList){
            setCarList(listCarData.carList?.slice(0 , pageParItem));
        } 
    } , [listCarData]);


    const showItem = async (car) => {
        setSelectedItem(car);
        setShowDitels(true);
    }

    return (
        <React.Fragment>
            {
                listCarData.loding ? (
                    <Spin size="large" tip="Loading..." />
                ) : (
                    <Col span={24} > 
                        {carList.map((car) => 
                            <Card key={car._id}  className="mb-3" bordered onClick={() => showItem(car)} >
                                <Row gutter={[8, 8]}>
                                    <Col span={5}>
                                        <Image src='./logo192.png'/>   { /* {car.image} for url path from DB now use demo PIC */  }
                                    </Col>
                                    
                                    <Col span={1} />

                                    <Col span={18}>
                                        <h4> {car.name} </h4>
                                        <p> {car.brand} </p>
                                        <p> {car.description}</p>
                                        <h5> ${ car.newprice ? car.newprice : car.price} </h5>
                                    </Col>
                                </Row>
                            </Card>
                        )}

                        <div className="new-car">
                            <Pagination 
                                defaultCurrent={1}  
                                pageSize={pageParItem} 
                                total={listCarData.carList?.length}
                                onChange={pageChange}
                            />   
                        </div>

                    </Col>
                       
                )
            }
            <Showcar showDitels={showDitels} setShowDitels={setShowDitels} selectedItem={selectedItem}/>
        </React.Fragment>
    );

};



export default Listcar;
