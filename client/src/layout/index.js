import React from 'react';
import '../styles/layout.css';
import { Layout } from "antd";
import Topbar from '../components/layout/Topbar';

const { Footer, Content } = Layout;

const MyLayout = ({ children }) => {

    return(

        <Layout style={{minHeight : '100vh'}}>
            <Topbar />

            <Content className="container-fluid no-padding no-margin">
                <div className="main-content-layout">
                    {children}
                </div>
            </Content>
            
            <Footer style={{ marginTop : 20, textAlign: 'center' , backgroundColor:'#fff' }}>Car Shop ©2021 Created by Jayanto Mondal</Footer>
        
        </Layout>
    );
};

export default MyLayout;