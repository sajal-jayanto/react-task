import React from 'react';
import Routers from '../src/routes/index';
import './App.css';

function App() {
    
    return (
        <React.Fragment>
            <Routers />
        </React.Fragment>
    );
}

export default App;
