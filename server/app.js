import express from 'express';
import { connect_mongodb } from './db.config.js';
import dotenv from 'dotenv';
import mongoose from 'mongoose';
import cors from 'cors';
import api from './src/api/index.js';


const app = express();

/// DB connection
connect_mongodb(mongoose);

app.use(cors());
app.use(express.json());


app.use('/api/v1', api());


dotenv.config();
const port = process.env.PORT || 5000;

app.listen(port , () => {
    console.log(`Sarver strat at port ${port}`);
});
