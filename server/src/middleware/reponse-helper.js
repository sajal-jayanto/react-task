
const STATUSES = {
    SUCCESS: 200,
    CREATED: 201,
};



const sendResponse = (res, data, status = STATUSES.SUCCESS) => res.status(status).json(data).end();


export const sendListResponse = (res , data) => sendResponse(res , data , STATUSES.SUCCESS);
export const sendCreateResponse = (res , data) => sendResponse(res , data , STATUSES.CREATED);
export const sendUpdatedResponse = (res , data) => sendResponse(res , data , STATUSES.SUCCESS);
export const sendDeleteResponse = res => sendResponse(res , { data : true } , STATUSES.SUCCESS);
