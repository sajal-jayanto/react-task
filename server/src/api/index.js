import express from 'express';

import car from '../controllers/car/index.js';

const routersInit = () => {
    
    const router = express();
    
    const publicRouter = express();
    publicRouter.use('/car' , car);
    router.use(publicRouter);

    // const privateRouter = express();
    // router.use(privateRouter);
    // privateRouter.use('/', );

    return router;
};


export default routersInit;
