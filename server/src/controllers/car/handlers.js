import express from 'express';
import multer from 'multer';
import mongoose from 'mongoose';
import Car from '../../models/car/index.js';
import {   
    
    sendListResponse, 
    sendCreateResponse , 
    sendUpdatedResponse,
    sendDeleteResponse 

} from '../../middleware/reponse-helper.js'

const storage = multer.diskStorage({
    destination : (req, file, cb) => cb(null, './image/'),
    filename : (req, file, cb) =>  cb(null, new Date().toISOString() + file.originalname)
});

const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') cb(null, true);
    else cb(null, false);    
}

const upload = multer({ 
    storage : storage, 
    limits: { fileSize: 1024 * 1024 * 5 },
    fileFilter : fileFilter 
});

const router = express.Router();

router.post('/list', async (req, res, next) => {
    
    try {
        const { word } = req.body
        const cars = await Car.find({brand : { "$regex" : word }});
        sendListResponse(res , cars);

    } catch (err) {
        next(err);
    }
});

router.get('/show/:id', async (req, res, next) => {
    
    try {

        const carId = req.params.id;
        const cars = await Car.find({ _id : mongoose.Types.ObjectId(carId)});
        sendListResponse(res , cars[0]);

    } catch (err) {
        next(err);
    }

});

router.post('/create', upload.single('file') , async (req, res, next) => {
    
    try {

        let { name , brand , image , description, price } = req.body;
        image = req.file.path;
        const car = new Car({ name , brand , image , description , price });
        await car.save();
        sendCreateResponse(res , car);

    } catch (err) {
        next(err);
    }

});

router.patch('/edit/:id', async (req, res, next) => {
    
    try {

        const updatedValue = req.body;
        const carId = req.params.id;
        await Car.findOneAndUpdate( { _id : carId }, updatedValue );
        const car = await Car.find({ _id : carId });
        sendUpdatedResponse(res , car); 

    } catch (err) {
        next(err);
    }

});

router.delete('/delete/:id', async (req, res, next) => {
    
    try {
    
        const carId = req.params.id;
        await Car.findByIdAndDelete({ _id : mongoose.Types.ObjectId(carId) });
        sendDeleteResponse(res);
        
    } catch (err) {
        next(err);
    }

});


export default router;
