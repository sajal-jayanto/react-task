import db from 'mongoose';

const { Schema, SchemaTypes } = db;

const schema = new Schema({
    
    name: {
        type: SchemaTypes.String,
        required: true
    },
    brand : {
        type: SchemaTypes.String,
        required: true
    },
    image : {
        type: SchemaTypes.String,
        required: true
    },
    description : {
        type: SchemaTypes.String,
        required: true
    },
    price : {
        type: SchemaTypes.String,
        required: true
    }
    
});

export default schema;
