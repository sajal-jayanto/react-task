import schema from './schema.js';
import mongoose from 'mongoose';

const Car = mongoose.model('Car', schema);

export default Car ;
