# React-task

### git config

`git clone https://gitlab.com/sajal-jayanto/react-task.git`
'''

# server setup

### Node js version 14.16.0 or More

### npm version 6.14.5

### yarn verison 1.22.5

### mongoDB as a Database

```
cd server
npm install
node app.js
```

### server is running at port 5000

# client setup

### node verison 14.16.0

### npm version 6.14.5

### yarn verison 1.22.5

```
cd client
yarn
yarn start
```

### Client server running at port 3000
